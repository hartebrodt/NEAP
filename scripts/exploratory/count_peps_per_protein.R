require(data.table)
require(optparse)
require(ggplot2)

option_list <- list(
  make_option(c("-e", "--evidence"), type="character",
              help="Proteome file relative to top level dir"),
  make_option(c("-o", "--outdir"), type="character",
              help="outdir")
)
opt <- parse_args(OptionParser(option_list=option_list))


dat<-fread(opt$evidence, sep="\t", header=T)
count<-dat[, .N, by=c("Leading Razor Protein")]
tissue<-gsub(".txt", "", basename(opt$evidence))
count$tissue<-tissue

fwrite(count, file.path(opt$outdir, paste0(tissue, "_peps_per_gene.tsv")), sep="\t")

pdf(file.path(opt$outdir, paste0(tissue, "_peps_per_gene_hist.pdf")))
p<-ggplot(count, aes(N))+geom_histogram(bins=100)+xlab("#Proteins")+ylab("#Peptides")+ggtitle("Number of peptides per protein")
plot(p)
plot(p+xlim(0,100))
dev.off()

