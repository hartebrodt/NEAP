require(data.table)
require(seqinr)
require(ggplot2)
require(stringr)
require(optparse)
option_list <- list(
  make_option(c("-p", "--proteome"), type="character",
              help="Proteome file relative to top level dir"),
  make_option(c("-t", "--peptides"), type="character",
              help="Peptide file"),
  make_option(c("-o", "--outdir"), type="character",
              help="Output directory"),
  make_option(c("-i", "--title"), type="character",
              help="Plot title")
)
opt <- parse_args(OptionParser(option_list=option_list))
#opt$proteome<-"~/NEAP/NEAP/data/human_proteome/Homo_sapiens.GRCh38.pep.all.deduplicated.tsv"
prot<-fread(opt$proteome, sep="\t", header=T)
prot[, len:=nchar(protein)]
longest_isofom<-prot[, transcript[which.max(len)], by=gene]
colnames(longest_isofom)<-c("gene", "transcript")


peptides<-fread(opt$peptides, header=T, sep="\t")
peptides$protein<-sapply(peptides$protein, function(x) unlist(str_split(x, "\\|")))

main<-peptides[transcript %in% longest_isofom$transcript]
peptides<-peptides[transcript %in% prot$transcript,]
minor<-peptides[!(transcript %in% longest_isofom$transcript)]
all_minor<-function(x){
    m<-minor[gene==x]
    l<-unlist(m$protein)
    l
}
minor_gene<-lapply(unique(minor$gene), function(x) all_minor(x))
names(minor_gene)<-unique(minor$gene)

#not actually the symetric difference...
symdif<-function(x){
    setdiff(unlist(minor_gene[x]), unlist(main[gene==x, ]$protein))
}
res<-lapply(names(minor_gene), function(x) symdif(x))
l<-sapply(res, function(x) length(x))
l_main<-sapply(names(minor_gene), function(x) length(unlist(peptides[gene==x, ]$protein)))
per<-l/as.numeric(l_main)
print(paste0("Minor isoform: ",sum(l)))
print(paste0("Total Number of peptides: ",sum(l_main)))

p<-ggplot()+geom_histogram(aes(x=per), bins=100)+ggtitle(paste0("Fraction of peptides belonging to any minor isoform of a gene\ndataset:", opt$title))+
  xlab("Fraction of peptides")+ylab("#Occurences")+theme_gray()

png(file.path(opt$outdir, paste0(opt$title, "_hist.png")), width=600, height=600)
plot(p)
dev.off()
fwrite(as.list(per), file.path(opt$outdir, paste0(opt$title, "_percs.tsv")), sep="\t")
