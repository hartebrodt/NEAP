require(data.table)
require(stringr)
library(ggplot2)
require(GenomicRanges)
require(seqinr)

require(optparse)

option_list <- list(
    make_option(c("-c", "--coverage"), type="character",
                help="Coverage file"),
    make_option(c("-o", "--outfile"), type="character",
                help="Coverage output file"),
    make_option(c("-p", "--proteome"), type="character",
                help="proteome")
)
opt <- parse_args(OptionParser(option_list=option_list))
#opt$proteome<-"~/Dokumente/NEAP/NEAP/data/human_proteome/uniprot2015.deduplicated.tsv"
#opt$coverage<-"/home/proj/biocluster/praktikum/neap_pearl/hartebrodt_lehner/project_data/results_uniprot_2015/03_coverage/Kim_Search_Results/adrenal.gland_evidence/adrenal.gland_evidence_coverage_unique.tsv"
print(opt$proteome)
print(opt$coverage)
map<-fread(opt$proteome, sep="\t", header=T)
cov<-fread(opt$coverage, header=T, sep="\t")
co<-colnames(cov)

longest<-function(ge){
  sub<-map[gene==ge,]
  sub[, len:=nchar(protein)]
  max(sub$len)
}
covi<-function(ge){
  sub<-cov[gene==ge,]
  mm<-c()
  for(i in 1:length(sub$start)){
    mm[as.numeric(sub$stop[i]):as.numeric(sub$start[i])]<-1
  }
  mm[is.na(mm)]<-0
  ma<-longest(ge)
  list(ge, sum(mm)/ma)
}


res<-lapply(unique(cov$gene), function(x) covi(x))
res<-as.data.table(do.call(rbind, res))
fwrite(res, opt$outfile, sep="\t")
