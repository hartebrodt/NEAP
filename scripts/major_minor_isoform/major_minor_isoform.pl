# @autor: nick
# Gets file with genes and peptides and finds major isoform for every gene
# Can use two different approaches: Greedy & Tress et al.
# Usage: "perl major_minor_isoform.pl -f <peptide-file> -o <desired output file> -h -g/-t"

use Getopt::Std;
use Data::Dumper;
#use strict;

if(@ARGV==0){	# test for the existence of the options on the command line
die("\nParameters to be used:
-f\tpath to input file
-e\tpath to evidence file (only if input files are not simulated)
-o\tpath to output file
-h\theader in input file present? Yes -> set \"-h\"
-g / -t\tapproach: -g -> greedy; -t -> Tress
-p\tpath to proteome file (for Tress approach)
-s\tsimulated files? Yes -> set \"-s\" AND leave out \"-e\"
-d\tscore column in evidence file (zero-based); only set if different from default (= column 47)!
  \tExample score columns: Kim/Wilhelm -> default. Lamond -> \"-d 55\"\n")}

# declare the perl command line flags/options we want to allow
my %options=();
getopts("f:o:hgtp:e:sd:", \%options);

#do# parameter checking & open files
print "\nParameters used:\n";

if(defined $options{f}){		# input file checking
	print "-f $options{f}\n";
	open(IN_M,	'<', $options{f}) or die("\nERROR: No input file found! Use relative path.\n"); #mapping file open
}
else{
		die("ERROR: Missing/incomplete parameter -f (must contain path to input file)\n");
}
if(defined $options{e}){	# evidence file open
	print "-e $options{e}\n";
	open(IN_E,	'<', $options{e}) or die("\nNo evidence file found! Use relative path.\n"); #mapping file open
	$simulation=0;
	if(defined $options{s}){
		die("ERROR: Don't use \"-s\" in combination with \"-e\"!\n");
	}
}
else{
	if(defined $options{s}){
		$simulation=1;
	}
	else{
		die("ERROR: Missing/incomplete parameter -e (must contain path to evidence file)\n");
	}
	
}
if(defined $options{o}){
	print "-o output in: $options{o}\n";
	open(OUT,	'>', $options{o});	#output file open
}
else{
	die("ERROR: Missing/incomplete parameter -o (must contain path to output file)\n");
}
if(defined $options{h}){		# header in mapping file yes or no
	print "-h option used. Header in input file present.\n";
	$count=0;
}
else{
	print "-h option NOT used. No header in input file present.\n";
	$count=1;
}
if(defined $options{g}){		# greedy
	print "-g option used. Greedy approach used.\n";
	$greedy=1;
	if(defined $options{p}){
		die("ERROR: Don't use \"-p\" in combination with \"-g\"!\n");
	}
}
elsif(defined $options{t}){		# Tress
	print "-t option used. Tress et al. approach used (longest isoform).\n";
	$tress=1;
	if(defined $options{p}){
		print "-p $options{p}\n";
		open(IN_P,	'<', $options{p}) or die("\nERROR: No proteome file found! Use relative path.\n");  #open proteome file
	}
	else{
		die("ERROR: Missing/incomplete parameter -p (must contain path to proteome file)\n");
	}
}
else{
	die("ERROR: Missing/incomplete parameter -g or -t (must specify approach)\n");
}
if(defined $options{d}){		# header in mapping file yes or no
	print "-d option used. Setting score column to $options{d}.\n";
	$custom_column=1;
}
else{
	print "-d option NOT used. Looking for score in default column 47.\n";
	$custom_column=0;
}
#done# parameter checking & open files


#do# load input file and save into hash %mapping
%mapping=();
while(<IN_M>){		
	chomp($_);
	my $line=$_;
	$count++;
	if ($count>1){	#skip header
		my @split_line=split("\t",$line);
		my $peptide=$split_line[4];
		my $gene_name = $split_line[0];	
		my $t1=$split_line[1];	#transcript 1
		$mapping{$gene_name}{$t1}{$peptide}=1;
	}
}
close IN_M;
#done# load input file and save into hash %mapping

#do# load evidence file and save into hash %mapping_evi
%mapping_evi=();
if($simulation==0){
	while(<IN_E>){	#line by line read of evidence file
		
		$count_ev_entries++;
		if ($count_ev_entries==1){		#save header (column names)
			chomp($_);
			my @split=split("\t",$_);
		#	$header="$split[1]\t$split[6]\t$split[7]\t$split[8]\t$split[9]\t$split[11]\t$split[18]\t$split[47]\t$split[48]";		#selected evidence info
			if($custom_column==1){
				$header = "$split[$options{d}]";
			}
			else{
				$header = "$split[47]";
			}

		}
		elsif ($count_ev_entries>1) {	#skip header

			chomp($_);
			my @split=split("\t",$_);
			my $peptide=$split[0];
			#my $info = "$split[1]\t$split[6]\t$split[7]\t$split[8]\t$split[9]\t$split[11]\t$split[18]\t$split[47]\t$split[48]";		#selected evidence info
			if($custom_column==1){
				$info = "$split[$options{d}]";
				$info = "".$info;
			}
			else{
				$info = "$split[47]";
			}


			$mapping_evi{$peptide}{$info}=1;
		}
	}
}
close IN_E;
#done# load evidence file and save into hash %mapping_evi
#print Dumper %mapping_evi;


#do# find transcript with most peptides mapped to it
if($greedy==1){		# greedy approach
	print OUT "Gene\tNumber_of_peptides_ON_major_isoform\tNumber_of_peptides_NOT_ON_major_isoform". #header
	"\tMain_Isoform\tMinor_Isoform(s)\tPeptides_on_major_isoform\tPeptides_NOT_ON_major_isoform".
	"\tScore(s)_major_peptides\tScore(s)_NOT_major_peptides\tDistinct_main_isoform\n";

	foreach my $gene_name (keys %mapping){
		#print "GENE NAME: $gene_name\n";
		$maxcount=0;
		undef(@all_peptides);
		undef(@major_peptides);
		undef(@scores_major);
		undef(@scores_major_string_all);
		undef(@scores_minor);
		undef(@scores_minor_string_all);
		undef(@all_peptides_unique);
		undef(@all_transcripts);
		$distinct=1;

		foreach my $transcript1 (keys %{$mapping{$gene_name}}){
			$count=0;
			push(@all_transcripts,$transcript1);
			foreach my $peptide (keys %{$mapping{$gene_name}{$transcript1}}){
				$count++;
				push(@all_peptides, $peptide);
			}

			if($count>$maxcount){
				$maxcount=$count;
				$best_transcript=$transcript1;
			}
			elsif($count==$maxcount){	# what if same number of peptides map to different transcripts?
				$distinct=0;
				if($transcript1 lt $best_transcript){		# if $transcript1 is alphabetically prior to $best_transcript -> $transcript1 is prefered as main-Isoform
					$best_transcript=$transcript1;
				}
			}
		}

		foreach my $peptide (keys %{$mapping{$gene_name}{$best_transcript}}){		#major isoform
			@all_peptides = grep(!/$peptide/, @all_peptides);
			push(@major_peptides,$peptide);
			@scores_major=keys %{$mapping_evi{$peptide}};
			$scores_major_string=join(";",@scores_major);
			push(@scores_major_string_all, $scores_major_string);
		}

		my @all_peptides_unique = do { my %seen; grep { !$seen{$_}++ } @all_peptides };		# remove redundant peptides in minor isoform
		foreach my $peptide (@all_peptides_unique){		# minor isoform(s)
			@scores_minor=keys %{$mapping_evi{$peptide}};
			$scores_minor_string=join(";",@scores_minor);
			push(@scores_minor_string_all, $scores_minor_string);
		}

		@all_transcripts = grep(!/$best_transcript/, @all_transcripts);		#remove best transcript from set of all transcripts for a $gene_name -> minor isoforms left
		$all_transcripts_string = join(";",@all_transcripts);		#minor isoforms
		my $peptide_count_rest = @all_peptides_unique;
		$major_peptides_string=join(";",@major_peptides);
		$minor_peptides_string=join(";",@all_peptides_unique);
		$major_peptides_scores=join("|",@scores_major_string_all);
		$minor_peptides_scores=join("|",@scores_minor_string_all);
		print OUT "$gene_name\t$maxcount\t$peptide_count_rest\t$best_transcript\t$all_transcripts_string\t$major_peptides_string\t$minor_peptides_string\t$major_peptides_scores\t$minor_peptides_scores\t$distinct\n";
	}
}
elsif($tress==1){		# Tress approach

	%mapping_prot=();
	while(<IN_P>){		#read in proteome file -> %mapping_prot
		chomp($_);
		my $line=$_;

		$skip++;
		if ($skip>1){	#skip header
			my @split_line=split("\t",$line);
			my $peptide=$split_line[2];
			my $gene_name = $split_line[0];	
			my $t1=$split_line[1];	#transcript 1

			$mapping_prot{$gene_name}{$t1}{$peptide}=1;
		}
	}
	close IN_M;

	print OUT "Gene\tNumber_of_peptides_ON_major_isoform\tNumber_of_peptides_NOT_ON_major_isoform". #header
	"\tMain_Isoform\tMinor_Isoform(s)\tPeptides_on_major_isoform\tPeptides_NOT_ON_major_isoform".
	"\tScore(s)_major_peptides\tScore(s)_NOT_major_peptides\tDistinct_main_isoform\n";
	
	foreach my $gene_name (keys %mapping){
		$longest_transcript=0;
		$peptide_count=0;
		$peptide_count_major=0;
		undef(@all_peptides);
		undef(@major_peptides);
		undef(@scores_major);
		undef(@scores_major_string_all);
		undef(@scores_minor);
		undef(@scores_minor_string_all);
		undef(@all_peptides_unique);
		undef(@all_transcripts);
		$distinct=1;

		#print "Gene: $gene_name\n";
		foreach my $transcript1 (keys %{$mapping{$gene_name}}){		#find transcript name in proteome mapping (%mapping_prot)
			@full_transcripts=keys %{$mapping_prot{$gene_name}{$transcript1}};	#find full transcript for given $gene_name+$transcript1
			$county=0;
			push(@all_transcripts,$transcript1);	#save all transcripts
			#print "\tTx: $transcript1\n";
			foreach $i (@full_transcripts){
				$full_transcript=$i;
				$county++;
				if($county>1){
					die("This should never happen! Two transcript sequences in one proteome-transcript entry: $gene_name\t$transcript1\n")
				}
			}

			foreach my $peptide (keys %{$mapping{$gene_name}{$transcript1}}){	#create set of all peptides for later
				push(@all_peptides, $peptide);
			}

			#print "\tFull Tx: $full_transcript\n";
			$transcript_length=length($full_transcript);
			#print "\tTx leng: $transcript_length\n";
			if($transcript_length>$longest_transcript){		#transcript longer than previously found ones
				$longest_transcript=$transcript_length;
				$longest_transcript_id=$transcript1;
			}
			elsif($transcript_length==$longest_transcript){	 	# equal length transcripts
				$distinct=0;
				if($transcript1 lt $longest_transcript_id){		# if $transcript1 is alphabetically prior to $best_transcript -> $transcript1 is prefered as main-Isoform
					$longest_transcript_id=$transcript1;										
				}

			}
		}

		foreach my $peptide (keys %{$mapping{$gene_name}{$longest_transcript_id}}){		#major isoform
			$peptide_count_major++;
			@all_peptides = grep(!/$peptide/, @all_peptides);
			push(@major_peptides,$peptide);
			@scores_major=keys %{$mapping_evi{$peptide}};
			$scores_major_string=join(";",@scores_major);
			push(@scores_major_string_all, $scores_major_string);
		}

		my @all_peptides_unique = do { my %seen; grep { !$seen{$_}++ } @all_peptides };		# remove redundant peptides in minor isoform
		foreach my $peptide (@all_peptides_unique){		# minor isoform(s)
			@scores_minor=keys %{$mapping_evi{$peptide}};
			$scores_minor_string=join(";",@scores_minor);
			push(@scores_minor_string_all, $scores_minor_string);
		}

		@all_transcripts = grep(!/$longest_transcript_id/, @all_transcripts);		#remove best transcript from set of all transcripts for a $gene_name -> minor isoforms left
		$all_transcripts_string = join(";",@all_transcripts);		#minor isoforms
		my $peptide_count_rest = @all_peptides_unique;		#number of unique peptides on minor isoform
		$major_peptides_string=join(";",@major_peptides);
		$minor_peptides_string=join(";",@all_peptides_unique);
		$major_peptides_scores=join("|",@scores_major_string_all);
		$minor_peptides_scores=join("|",@scores_minor_string_all);

	print OUT "$gene_name\t$peptide_count_major\t$peptide_count_rest\t$longest_transcript_id\t$all_transcripts_string\t$major_peptides_string\t$minor_peptides_string\t$major_peptides_scores\t$minor_peptides_scores\t$distinct\n";
	#print "\tPeptides on minor isoform: $peptide_count\n"
	}
}
##done## find transcript with most peptides mapped to it

print "\n";