# @autor: nick
# Gets mapping-file and appends major isoform info for every peptide
# Usage: "perl append_mm_info_to_mapping.pl -f <mapping-file> -m <major-minor-file> -o <desired output file>"

use Getopt::Std;
use Data::Dumper;

if(@ARGV==0){	# test for the existence of the options on the command line
die("\nParameters to be used:
-f\tpath to mapping file
-m\tpath to major-minor-isoform file
-o\tpath to output file\n")}
#-b/-t\t-b for broad/naive identification; or -t for Tress et al. identification
#-p\tpath to transcriptome file

# declare the perl command line flags/options we want to allow
my %options=();
getopts("f:o:m:", \%options);

#do# parameter checking & open files
print "\nParameters used:\n";

if(defined $options{f}){		# parameter flag checking
	print "-f $options{f}\n";
	open(IN_Map,	'<', $options{f}) or die("\nNo mapping file found! Use relative path.\n"); #mapping file open
}
else{
		die("ERROR: Missing/incomplete parameter -f (must contain path to mapping file)\n");
	}
if(defined $options{m}){		# parameter flag checking
	print "-m $options{m}\n";
	open(IN_Minor,	'<', $options{m}) or die("\nNo major-minor-isoform file found! Use relative path.\n"); #mapping file open
}
else{
		die("ERROR: Missing/incomplete parameter -f (must contain path to major-minor-isoform file)\n");
	}
if(defined $options{o}){
	print "-o $options{o}\n";
	open(OUT,	'>', $options{o});	#output file open
}
else{
	die("ERROR: Missing/incomplete parameter -o (must contain path to output file)\n");
	}
#done# parameter checking & open files

#do# load major-minor-isoform file and save into hash %mapping
$header="";
$count=0;
%mapping=();
while(<IN_Minor>){		
	chomp($_);
	my $line=$_;
	$count++;
	if ($count>1){	#skip header
		my @split_line=split("\t",$line);
		my $peptide=$split_line[0];
		#my $gene_name = $split_line[1];	
		#my $t1=$split_line[2];	#transcript 1
		my $t_major=$split_line[3];	#major transcript
		#shift @split_line for 1..4;
		#my $evidence = join("\t",@split_line);

		$mapping{$peptide}={$t_major};#{$gene_name}{$t1}=1;
	}
}
close IN_Minor;
#done# load major-minor-isoform file and save into hash %mapping

#do# load mapping file and save into hash %mapping
$count=0;
while(<IN_Map>){		
	chomp($_);
	my $line=$_;
	$count++;
	if ($count>1){	#skip header
		my @split_line=split("\t",$line);
		my $peptide=$split_line[0];
		# my $gene_name = $split_line[1];	
		# my $t1=$split_line[2];	#transcript 1
		# my $t2=$split_line[3];	#transcript 2
		# shift @split_line for 1..4;
		# my $evidence = join("\t",@split_line);
		
		if (exists $mapping{$peptide}) {
			@majors=keys %{$mapping{$peptide}};
			foreach $i (@majors){
				$major=$i;
			}
			print OUT "$line\t$major\n";
		}
	}
	else{
		print OUT "$line\tMajor_isoform\n";
	}
}
close IN_Map;
#done# load mapping file and save into hash %mapping

print "\n";