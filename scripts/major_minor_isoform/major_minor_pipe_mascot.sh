#!/bin/bash

# script to count peptide matches on major or minor isoform for results/02_mapping_to_evidences/trypsin_l50_m2/kim and results/02_mapping_to_evidences/trypsin_l50_m2/wilhelm
# two different strategies: -t / -g
# usage example: "bash major_minor.sh <my_output_file_name(no file ending)> -t/-g -h <proteome_file.tsv(only with -t)>"

i_end=''
data=""
output=$2
approach=$3
header=$4
evidence=$5
lab=$6
if [[ "$#" -ne 7 ]];
then
proteome=$7
scriptfolder=$8
else
scriptfolder=$7
fi
#echo $scriptfolder

for i in $1/* ; do 	#kim data set
        if [[ $i =~ "_evidence" ]]; then
                evi_cov="_coverage_unique.tsv"
        	evi_evi='_evidence.txt'
                i_end=${i##*/}          #cut for example ../results/02_mapping_to_evidences/trypsin_l50_m2/kim/pancreas_evidence -> pancreas_evidence
                i_end_no_evidence=${i_end%_*}
                i_end_no_evidence_plus_csv=$i_end_no_evidence".csv"
                i_end_folder=${1##*/}
                data=$i_end_folder
                #i_end_no_evidence_plus_evi=$i_end_no_evidence$evi_evi	#concat strings: pancreas -> pancreas_evidence.txt
                i_end_plus_evi_cov=$i_end$evi_cov	#concat strings: pancreas -> pancreas_coverage_unique.tsv
                proteome_name=${proteome##*/}           #proteome name without path
                proteome_name_no_tsv=${proteome_name%%.tsv}
                i_end_plus_approach=$i_end_no_evidence"_"$output"_"$proteome_name_no_tsv".tsv"	#concat strings: pancreas -> pancreas_$output

                mkdir -p /home/nick/local/biocluster/praktikum/neap_pearl_ss17/hartebrodt_lehner/project_data/move_me_to_results_ensembl_final/10_major_minor_isoform/mascot/$i_end_no_evidence

                perl $scriptfolder/scripts/major_minor_isoform/major_minor_isoform.pl -d 2 -f $1/$i_end/$i_end_plus_evi_cov -e $evidence/unix/$i_end_no_evidence_plus_csv -o /home/nick/local/biocluster/praktikum/neap_pearl_ss17/hartebrodt_lehner/project_data/move_me_to_results_ensembl_final/10_major_minor_isoform/mascot/$i_end_no_evidence/$i_end_plus_approach $approach $header -p $proteome
        fi
done

echo "========== Data set $data $approach finished =========="
