from Bio import SeqIO
import ahocorasick
import os.path
import argparse
import re
parser = argparse.ArgumentParser(description='Read Coverage')
parser.add_argument('inputfile', metavar="Input", type=str,
                                        help='evidence file')
parser.add_argument('outdir', metavar="Input", type=str,
                                       help='outdir file')
parser.add_argument('protfile', metavar="Proteome", type=str,
                                        help='proteome file')
parser.add_argument('mappingfile', metavar="IdMapping", type=str,
                                        help='mapping file UniProt->ENST*')                                        
parser.add_argument('outfile', metavar="Name main outputfile", type=str,
                                        help='proteome file')
args = parser.parse_args()
outdir=args.outdir


if not os.path.exists(args.outdir):
    os.makedirs(args.outdir)

inputfile=args.inputfile
evid={}
# 11: razor protein 12: gene name
# make a mapping of protein name : [list of peptides] (make a list to keep frequency information)
with open(inputfile, "r") as handle:
    next(handle)
    for line in handle:
        l=line.rstrip().split("\t")
        ll=l[11] # razor protein
        #ll=re.sub("-[0-9]", "", ll)
        if ll not in evid:
            evid[ll]=[l[0]]
        else:
            evid[ll].append(l[0])


#Get the mapping of the Uniprot ID to the ENST* ID
ma= {}
# Protein name -> Transcript mapping downloaded from uniprot
with open(args.mappingfile, "r") as handle:
    for line in handle:
        l=line.rstrip().split("\t")
        #s=re.sub("-[0-9]", "", l[1]) Do not even try
        s=l[1]
        if l[0] not in ma.keys():
             ma[l[0]]=set([s])
        else:
            ma[l[0]].add(s)
#print(set(evid.keys()).intersection(set(ma.keys())))
#print(len(set(evid.keys()).intersection(set(ma.keys()))))


# Get the proteome as a dict with ENST* ids as keys
records = SeqIO.to_dict(SeqIO.parse(args.protfile, "fasta"))
prot={}
tra=0
for rec in records.keys():
    #k = records[rec].description.split(" ")[7].replace("gene_symbol:", "")
    t = records[rec].description.split(" ")[4].replace("transcript:", "")
    t= re.sub("\.[0-9]*", "", t)
    if not t in prot.keys():
        prot[t]= records[rec].seq
    else:
        tra=tra+1
#print("transcript id found multiple times: "+str(tra))

#print(len(list(records.keys())))

res=[]
non_found_transcript=[]
non_found_protein=[]
#prot: proteins that have peptides
for protein in evid.keys():
    # Peptides for the protein proteins
    for pep in evid[protein]:
        # transcripts for this protein
        try:
            for trans in ma[protein]:
                try:
                    idx=[(m.start(), m.end()) for m in re.finditer(str(pep), str(prot[trans]))]
                    if idx!=[]:
                        res.append([trans, pep, idx, str(prot[trans])])
                except KeyError:
                    non_found_transcript.append(trans)
                    continue
        except KeyError:
            if "CON" not in protein and "REV" not in protein:
                non_found_protein.append([protein, pep])
#print(prot.keys())

with open(os.path.join(args.outdir, args.outfile), "w") as handle:
    for r in res:
        handle.write(r[0]+"\t"+r[1]+"\t"+str(r[2][0][0])+"\t"+str(r[2][0][1])+"\t"+r[3]+"\n")
with open(os.path.join(args.outdir, "ENST_missing.txt"), "w") as handle:
    for r in non_found_transcript:
        handle.write(r+"\n")
with open(os.path.join(args.outdir, "UniProt_id_missing.txt"), "w") as handle:
    for r in non_found_protein:
        handle.write(r[0]+"\t"+r[1]+"\n")
