
# coding: utf-8

# In[2]:

import ahocorasick
from Bio import SeqIO
import argparse
import os.path
import re

parser = argparse.ArgumentParser(description='Read Coverage')
parser.add_argument('inputfile', metavar="Input", type=str,
                                        help='evidence file')
parser.add_argument('outdir', metavar="Input", type=str,
                                       help='outdir file')
parser.add_argument('protfile', metavar="Proteome", type=str,
                                        help='proteome file')
#parser.add_argument('mappingfile', metavar="IdMapping", type=str,
#                                        help='mapping file UniProt->ENST*')
parser.add_argument('outfile', metavar="Name main outputfile", type=str,
                                        help='proteome file')
parser.add_argument('prot_format', metavar="Proteome format", type=str, help='proteome format')
args = parser.parse_args()
outdir=args.outdir


if not os.path.exists(args.outdir):
    os.makedirs(args.outdir)


# In[18]:

#protfile = "/home/anne/NEAP/NEAP/data/human_proteome/Homo_sapiens.GRCh38.pep.all.fa"
records = SeqIO.to_dict(SeqIO.parse(args.protfile, "fasta"))
prot={}
tra=0
mapping={}
if args.prot_format=="ensembl":
    for rec in records.keys():
        #k = records[rec].description.split(" ")[7].replace("gene_symbol:", "")
        g = records[rec].description.split(" ")[3].replace("gene:", "")
        t = records[rec].description.split(" ")[4].replace("transcript:", "")
        mapping[t]=g
        if not t in prot.keys():
            prot[t]= records[rec].seq
        else:
            tra=tra+1
else:
    if args.prot_format == "uniprot":
        for rec in records.keys():
            #k = records[rec].description.split(" ")[7].replace("gene_symbol:", "")
            g = re.findall("GN=[0-9A-Za-z\\-]*", records[rec].description)
            if len(g)==0:
                continue
            g = g[0].split("=")[1]
            t = records[rec].description.split("|")[1]
            mapping[t]=g
            if not t in prot.keys():
                prot[t]= records[rec].seq
            else:
                tra=tra+1


# In[19]:

A = ahocorasick.Automaton()
#evidencefile="/home/anne/NEAP/project/rerun_pancreas_evidence.txt"
with open(args.inputfile, "r") as handle:
    for line in handle:
        l= line.strip("\n").split("\t")
        A.add_word(l[0], (len(l[0]),l[0]))
A.make_automaton()


# In[20]:

t={}
for i in prot.keys():
    it=A.iter(str(prot[i]))
    if len(set(it)) > 0:
        t[i]=A.iter(str(prot[i]))




# In[21]:

result=[]
for tr in t.keys():
    for it in t[tr]:
        idx= it
        result.append([mapping[tr], tr, it[0]-it[1][0]+1, it[0], it[1][1] ])



# In[22]:

with open(os.path.join(args.outdir, args.outfile) , "w") as handle:
    for r in result:
        for i in r:
            handle.write(str(i))
            handle.write("\t")
        handle.write("\n")
