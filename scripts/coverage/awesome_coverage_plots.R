require(data.table)
require(Pviz)
require(seqinr)
require(Biostrings)
require(plotly)
require(stringr)
require(GenomicFeatures)
require(optparse)

option_list <- list(
    make_option(c("-p", "--proteome"), type="character",
                help="Proteome file relative to top level dir"),
    make_option(c("-c", "--coverage"), type="character",
                help="Coverage file"),
    make_option(c("-o", "--outdir"), type="character",
                help="Output directory"),
    make_option(c("-r", "--proteome_ranges"), type="character",
                help="Proteome ranges file")
)
opt <- parse_args(OptionParser(option_list=option_list))

source("~/NEAP/NEAP/scripts/coverage/color_scheme.R")
opt$coverage<-"~/NEAP/project/coverage_kim_rerun_uni.tsv"
opt$proteome<-"~/NEAP/NEAP/data/human_proteome/Homo_sapiens.GRCh38.pep.all.fa"
opt$proteome_ranges<-"~/NEAP/NEAP/data/human_proteome/petide_ranges.tsv"
d_count<-fread(opt$coverage, header=T, sep="\t")
proteome<-read.fasta(opt$proteome, seqtype = "AA", as.string = T)
peptide_ranges<-fread(opt$proteome_ranges, header=T, sep="\t")


co <-colnames(d_count)

fun<-function(x){
  c(
    str_sub(attr(x, "Annot"),str_locate(attr(x, "Annot"), "gene[0-9A-Z:.]*")), 
    str_sub(attr(x, "Annot"),str_locate(attr(x, "Annot"), "transcript[0-9A-Z:.]*")), 
    x[1]
)
}

seqs<-lapply(proteome, function(x) fun(x))
seqs<-do.call(rbind, seqs)
seqs<-as.data.frame(seqs)
prot_seqs<-AAStringSet(seqs$V3)
names(prot_seqs)<-sapply(seqs$V2, function(x) gsub("transcript:", "", x))
#names(prot_seqs)<-sapply(names(prot_seqs), function(x) gsub("\\.[0-9]*", "", x, perl=T))
t<-unique(d_count$tx)
prot_seqs<-prot_seqs[t]





pep_wrapper<-function(txname, peptide_ranges){
pep<-peptide_ranges[transcript==txname]
pep[, width:=(V2-V1)/3]
pep
}
shifter<-function(x, probe){
idx<-which(probe$start>x[[1]])
probe$start[idx]<-probe$start[idx]+(x[[2]]-x[[1]])+1
probe$stop[idx]<-probe$stop[idx]+(x[[2]]-x[[1]])+1
probe
}
shift_wrapper<-function(shift, probe){
for(i in 1:nrow(shift)){
  probe<-shifter(shift[i,], probe)
}
probe
}
probe_merger<-function(probes1, probes2){
p<-merge(probes1[,1:4], probes2[,1:4], by=c("tx", "start", "stop"), all=T, allow.cartesian=T)
p[, intens:=rep(1,nrow(p))]
p$intens[which(is.na(p$gene.x))]<-2
p$intens[which(is.na(p$gene.y))]<-3
p
}
boundary<-function(pep, shift){
  ir<-IRanges(pep$V1, pep$V2)
  boundaries<-ir@width/3
  boundaries<-sapply(1:length(boundaries), function(x) sum(boundaries[1:x]))
  if(nrow(shift)!=0){
  for (s in 1:nrow(shift)){
    boundaries[boundaries>as.integer(shift[s,1])]<-boundaries[boundaries>as.integer(shift[s,1])]+as.integer(shift[s,2])-as.integer(shift[s,1])+1
  }
  }
  boundaries<-c(1, boundaries)
  round(boundaries, digits=0)
}
seq_wrapper<-function(seq, boundaries){
  seq<-ProteinSequenceTrack(seq)
  ht<-HighlightTrack(trackList=seq, start=boundaries, width=0)
  ht
}
gene_name="ENSG00000165280.15"
tx1_name="ENST00000377488.5"
tx2_name="ENST00000377493.9"
peptide_ranges[gene==gene_name]
pep1<-pep_wrapper(tx1_name, peptide_ranges)
pep2<-pep_wrapper(tx2_name, peptide_ranges)
tx1<-prot_seqs[tx1_name]
tx2<-prot_seqs[tx2_name]
ali<-pairwiseAlignment(tx1,tx2)
probes2<-d_count[tx==tx2_name]
probes1<-d_count[tx==tx1_name]
shift1<-as.data.table(do.call(rbind,str_locate_all(as.character(pattern(ali)), "-+")))
shift2<-as.data.table(do.call(rbind,str_locate_all(as.character(subject(ali)), "-+")))
probes1<-shift_wrapper(shift1, probes1)
probes2<-shift_wrapper(shift2, probes2)
p<-probe_merger(probes1,probes2)
boundaries1<-boundary(pep1, shift1)
boundaries2<-boundary(pep2, shift2)
lili<-list()
lili["seq1"]<-seq_wrapper(pattern(ali), boundaries1)
lili["seq2"]<-seq_wrapper(subject(ali), boundaries2)
lili["probe1"]<-ProbeTrack(sequence=p$V2, probeStart=p$V3, intensity=p$intens, cex=0, color=c("#ff9900", "#990000", "#006600"))
lili["axisTrack"]<-ProteinAxisTrack()
plotTracks(lili, from =0, to=767,fontcolor=fcol)

