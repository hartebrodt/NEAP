require(data.table)
require(seqinr)
require(optparse)
require(stringr)

option_list <- list(
    make_option(c("-p", "--proteome"), type="character",
                help="Proteome file relative to top level dir"),
    make_option(c("-t", "--peptides"), type="character",
                help="Coverage file")
)
opt <- parse_args(OptionParser(option_list=option_list))
#opt$proteome<-"/home/proj/biocluster/praktikum/neap_pearl/hartebrodt_lehner/project_data/results_uniprot_2015/01_in_silico_digest/trypsin/trypsin_peptides.tsv"
#opt$peptides<-"/home/proj/biocluster/praktikum/neap_pearl/hartebrodt_lehner/project_data/results_uniprot_2015/01_in_silico_digest/trypsin/trypsin_peptides_6to50.tsv"

peps<-fread(opt$proteome, sep="\t", header=T)
peps$protein<-lapply(peps$protein, function(x) str_split(x, "\\|"))
peps$protein<-lapply(peps$protein, function(x) x[[1]][sapply(x[[1]], function(y) nchar(y)>=6 && nchar(y)<50 )])
fwrite(peps, opt$peptides , sep="\t")
