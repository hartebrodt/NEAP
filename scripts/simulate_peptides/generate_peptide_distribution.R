require(data.table)
require(optparse)
require(ggplot2)

option_list <- list(
  make_option(c("-m", "--mapping"), type="character",
              help="Mapping file"),
  make_option(c("-o", "--outdir"), type="character",
              help="outdir"),
  make_option(c("-e", "--evidence"), type="character",
              help="Evidence file to sample from")
)
opt <- parse_args(OptionParser(option_list=option_list))
#opt$evidence<-"~/Dokumente/NEAP/NEAP/results/06_peptides_per_protein/kim/data/colon_evidence_peps_per_gene.tsv"
#opt$mapping<-"~/Dokumente/NEAP/NEAP/data/id_mapping/mapping_to_gene.tsv"
sam<-fread(opt$evidence, sep="\t", header=T)
mapping<-fread(opt$mapping, sep="\t", header=T)

#Can we just ignore proteins that did not map? -> we are going to do just that
pdf(file.path(opt$outdir, "plots_dist_mapped_not_mapped.pdf"))
ma<-which(sam$"Leading Razor Protein" %in% mapping$From)
sa<-sam[ma]
man<-which(!(sam$"Leading Razor Protein" %in% mapping$From))
san<-sa[man]
ggplot(sa, aes(N))+geom_histogram()+xlim(0,155)+ggtitle(paste0("Distribution of peptides per protein in proteins that mapped an ENSP* id in", basename(opt$evidence)))+
  xlab("Number of peptides")+ylab("Number of Proteins")
ggplot(san, aes(N))+geom_histogram()+xlim(0,155)+ggtitle(paste0("Distribution of peptides per protein in proteins that did not map an ENSP* id", basename(opt$evidence)))+
  xlab("Number of peptides")+ylab("Number of Proteins")
dev.off()

m<-merge(sam, mapping, by.x="Leading Razor Protein", by.y="From")
count<-m[, sum(N), by=c("To")]
fwrite(count, file.path(opt$outdir, "peps_per_gene_dist.tsv"), sep="\t")