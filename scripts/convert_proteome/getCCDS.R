require(seqinr)
require(data.table)

prot<-read.fasta("/home/h/hartebrodt/Dokumente/NEAP/NEAP/data/human_proteome/Homo_sapiens.GRCh38.pep.deduplicated.fa", seqtype="AA", as.string=T)
ids<-read.table("~/Dokumente/NEAP/NEAP/data/CCDS/CCDS_Protein_IDS.txt", header=F)

prot_CCDS<-prot[ids$V1]
n<-sapply(prot_CCDS, function(x) gsub(">", "", attr(x, "Annot")))

write.fasta(prot_CCDS, names=n, file.out="/home/h/hartebrodt/Dokumente/NEAP/NEAP/data/human_proteome/Homo_sapiens.GRCh38.pep.CCDS.fa")
