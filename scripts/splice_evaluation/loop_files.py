
# coding: utf-8

# In[22]:

import os
import re
import subprocess
import argparse

parser = argparse.ArgumentParser(description='In silico digestion')
parser.add_argument('indir1', metavar="Input", type=str,
                                        help='Directories for all vs all comparison')
parser.add_argument('indir2', metavar="Input", type=str,
                                        help='Directories for all vs all comparison')
parser.add_argument('file_type1', metavar="filetype", type=str,
                                        help='Type of file to be compared e.g "longest"')
parser.add_argument('file_type2', metavar="filetype", type=str,
                                        help='Type of file to be compared e.g "longest"')
parser.add_argument('equal', metavar="equal_tissue", type=str,
                                        help='Equal tissues only')
parser.add_argument('outfolder', metavar="output directory", type=str,
                                        help='outdir')
parser.add_argument('outfile_stats', metavar="Individual stat files", type=str,
                                        help='outdir')
parser.add_argument('outfile_splice', metavar="Splice comparison", type=str,
                                        help='outdir')
parser.add_argument('first_name', metavar="Name first dataset for plots", type=str,
                                        help='outdir')
parser.add_argument('second_name', metavar="Name second dataset for plots", type=str,
                                        help='outdir')
parser.add_argument('name_plot_files', metavar="Name plot files", type=str,
                                        help='outdir')
parser.add_argument('base_dir', metavar="Base dir", type=str,
                                        help='outdir')


args = parser.parse_args()
outfolder=args.outfolder
if not os.path.exists(outfolder):
    os.makedirs(outfolder)
indir1=args.indir1
indir2=args.indir2
if(args.equal == "equal"):
    eq=True
if(args.equal == "all"):
    eq=False


# Every tissue against every tissue in the diectories
# If you want to make the tissue wise comparison across multiple tissue folders must have the same names

# In[27]:

sub1=os.listdir(indir1)
sub2=os.listdir(indir2)
sub1.sort()
sub2.sort()
#Move items that are not in the other list to the end of the list
for i in range(len(sub1)-1, -1, -1):
    if sub1[i] not in sub2:
        sub1.append(sub1.pop(i))
for i in range(len(sub2)-1, -1, -1):
    if sub2[i] not in sub1:
        sub2.append(sub2.pop(i))


for i in range(0, len(sub1)):
    if len(os.listdir(os.path.join(str(indir1),str(sub1[i]))))==0:
        continue
    fi=list(filter(lambda x: args.file_type1  in x, os.listdir(os.path.join(str(indir1),str(sub1[i])))))[0]
    for j in range(i, len(sub2)):
        if len(os.listdir(os.path.join(str(indir2),str(sub2[j]))))==0:
            continue
        if eq:
            if sub1[i]!=sub2[j]:
                continue
        fj=list(filter(lambda x: args.file_type2 in x, os.listdir(os.path.join(str(indir2),str(sub2[j])))))[0]
        pj=os.path.join(str(indir2), str(sub2[j]), fj)
        pi=os.path.join(str(indir1), str(sub1[i]), fi)
        si=os.path.join(outfolder, str(sub1[i]))
        sj=os.path.join(outfolder, str(sub2[j]))
        if not os.path.exists(os.path.join(outfolder,"scores")):
            os.makedirs(os.path.join(outfolder,"scores"))
        cmd= " ".join(["Rscript", args.base_dir +"/scripts/splice_evaluation/batch_version.R -e", pi,"-f", pj, "-s",
              os.path.join(args.outfolder, "scores", "scores_"+fi), "-t",os.path.join(args.outfolder, "scores", "scores_"+fj),"-o",
              os.path.join(outfolder, args.outfile_stats), "-c", os.path.join(outfolder, args.outfile_splice), "-b", args.base_dir, "-k", os.path.join(outfolder,args.name_plot_files)])
        subprocess.call(cmd, shell=True)
print("hello!")
cmd=" ".join(["Rscript", args.base_dir +"/scripts/splice_evaluation/batch_visualizer.R", "-f", args.first_name, "-s", args.second_name, "-i",  os.path.join(outfolder, args.outfile_stats),
            "-t", os.path.join(outfolder, args.outfile_splice), "-o", args.outfolder, "-n", args.name_plot_files])
print(cmd)
subprocess.call(cmd, shell=True)
