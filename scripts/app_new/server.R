library(shiny)
require(rtracklayer)
require(data.table)
require(Pviz)
require(plotly)
require(ggplot2)
require(RColorBrewer)
require(viridis)

options(shiny.maxRequestSize=4000*1024^2)
# global variables defined for the session
#gencode annotation
annotation<-NULL
#proteome in tsv format
proteome<-NULL
coverage<-NULL
coverages_perc<-NULL
proteases<-c()
evidence<-NULL
other_evidences<-NULL
insilico_cum<-NULL
splice_table<-NULL

# Define server logic required to draw a histogram
shinyServer(function(input, output, session){
    
#############################
# STUFF FOR TRANSCRIPTOME TAB
#############################
output$transcriptome<-renderPlotly({
    trans<-fread(file.path(basePath, "exploratory/transcriptome/transcripts_per_gene.tsv"), header=T, sep="\t")
            g<-ggplot(trans, aes(N))+geom_histogram(bins=max(trans$N))+
                ggtitle("Number of alternative transcripts per gene")+
                xlab("#Transcripts")+ylab("#Genes")
            ggplotly(g)

    } )
observe({
    if(is.null(insilico_cum)){
    insilico<-fread(file.path(basePath, "exploratory/perc_minor_isoform/cummulative_perc_minor_iso_all_proteases.tsv"), header=T, sep="\t")
    pal<-brewer.pal(n=length(unique(insilico$variable)), name="Spectral")
    names(pal)<-unique(insilico$variable)
    protea<-c(proteases, unique(insilico$variable))
    proteases<<-protea
    insilico_cum<<-insilico
    updateCheckboxGroupInput(session, "proteases_sel", "Select Proteases", choices=protea, selected = protea)
    }
    })

output$insipeplen<-renderPlot({
    if(is.null(input$proteases_sel)){
        return(NULL)
    }
    print(head(insilico_cum))
    insilico<-insilico_cum[variable %in% input$proteases_sel,]
    g<-ggplot(insilico, aes(x=value, color=variable)) +geom_step(stat="ecdf")+
        ggtitle("Cumulative distributions of the fraction of minor isoform peptides in the in-silico digests")+
        xlab("Fraction if minor isoform peptides")+ylab("%Genes")
    g
})



############################
# EVIDENCE EXPLORER
############################
output$evidence_pep_len<-renderPlotly({
    evid<-input$evidence
    if(is.null(evid)){
        return(NULL)
    }
    evid<-fread(evid$datapath, sep="\t", header=T)
    evidence<<-evid
    evid[,len:=nchar(Sequence)]
    g<-ggplot(evid, aes(len))+geom_histogram(bins=(max(evid$len)-min(evid$len)+1))+ggtitle("Peptide Length in Evidence")+
        xlab("Peptide Length")+ylab("#Peptides")
    ggplotly(g)
    
})

output$evid_comp_ins<-renderPlot({
    ins<-input$in_silico_pep_len
    input$evidence
    if(is.null(ins)){
        return(NULL)
    }
    if(is.null(evidence)){
        print("ha")
        return(NULL)
        
    }
    df<-fread(ins$datapath, header=T, sep="\t")
    evid<-evidence[,len:=nchar(Sequence)]
    evid<-evidence[, variable:="emp"]
    g<-ggplot(df, aes(x=value,color=variable)) +geom_step(stat="ecdf")+
        geom_vline(xintercept = 6)+
        coord_cartesian(xlim=c(0,50))+
        ggtitle("Peptide length distribution in-silico digests")+
        ylab("%Peptides")+xlab("Peptide length")+theme(text = element_text(size=20))+
    geom_step(data=evid, aes(x=len, color=variable), stat = "ecdf")
    g
})
output$evid_comp_evid<-renderPlot({
    inFiles<-input$other_evidences
    input$update_ev
    if(is.null(inFiles)){
        return(NULL)
    }
    other_evid<-concatFiles(inFiles, other_evidences)
    other_evid<-other_evid[, len:=nchar(Sequence)]
    g<-ggplot(other_evid, aes(x=len,color=dataset)) +geom_step(stat="ecdf")+
        ggtitle("Peptide length distribution evidences")+
        ylab("%Peptides")+xlab("Peptide length")+theme(text = element_text(size=20))
    if(!is.null(evidence)){
    evid<-evidence[,len:=nchar(Sequence)]
    evid<-evidence[, dataset:="your_data"]
    g<-g+geom_step(data=evid, aes(x=len, color=dataset), stat="ecdf")
    }
    g
    
})
observe({
    input$delete_evidence
    other_evidences<<-NULL
})

###########################
#COVERAGE BOXPLOT TAB
###########################
observe({
    fi<-input$coverage_other
    if(is.null(fi)){
        return(NULL)
    }
    co<-concatFiles(fi, coverages_perc)
    print(head(co))
    cho<-unique(co$dataset)
    updateCheckboxGroupInput(session,"dataset_sel", "Select Datasets", choices=cho, selected = cho)
    coverages_perc<<-co
})
output$coverageBoxplot<-renderPlotly({
    if(is.null(input$dataset_sel)){
        return(NULL)
    }
    if(is.null(coverages_perc)){
        return(NULL)
    }
    w<-input$dataset_sel
    co<-coverages_perc[dataset %in% w,]
    g<-ggplot(co, aes(x=dataset, y=V2))+geom_boxplot()+ggtitle("Sequence coverage (Fraction of longest isoform)")+
        ylab("%Sequence coverage (Fraction of AA covered)")+xlab("Dataset")
    ggplotly(g)
})
observe({
    input$delete_cov_per
    coverages_perc<<-NULL
    isolate(input$dataset_sel)
})

############################
# SPLICE EVENT PANEL
############################

observe({
    if(!is.null(input$splice_file)){
        splice_events<-concatFiles(input$splice_file, splice_table)
        splice_table<<-splice_events
        cho<-c(unique(splice_table$dataset))
        updateSelectInput(session,"first", "Select Condition", choices = cho, selected = cho)
        updateSelectInput(session,"second", "Select Condition", choices = cho, selected = cho)
    }
})
observe({
    input$delete_splice
    splice_table<<-NULL
})

output$splice_venn<-renderPlot({
    if(input$second=="" || input$first==""){
        return(NULL)
    }
    else{
    draw_diagram(splice_table, input$first, input$second, TRUE)
    }
})
output$pie<-renderPlot({
    if(input$second=="" || input$first==""){
        return(NULL)
    }
    else{
    same_major_isoform(splice_table, input$first, input$second)
    }
})
output$bar<-renderPlot({
    if(is.null(splice_table)){
        return(NULL)
    }
    tab<-tabulateSplicing(splice_table)
    print(tab)
    
})
output$protein_overlap<-renderPlot({
        if(input$second=="" || input$first==""){
            return(NULL)
        }
        else{
            draw_diagram(splice_table, input$first, input$second, FALSE)
        }
})

output$splice_summary<-renderPlot({
    input$update_barplot
    if(is.null(splice_table)){
        return(NULL)
    }
    tab<-tabulateSplicing(splice_table)
    g<-ggplot(tab, aes(y=frac, x=dataset))+geom_bar(stat = "identity")+theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust=0.5))+
        xlab("Dataset")+ylab("%Peptides Mapping To Minor Isoform")+ggtitle("Summary of %Minor Isoform Peptides")+coord_flip()
    g
})

output$compare_scores<-renderPlot({
    input$update_barplot
    sco<-scorecery(splice_table)
    print(sco)
    g<-ggplot(sco, aes(y=sco$scores, x=dataset, fill =isoform))+geom_boxplot()+ggtitle("Comparison of Scores in Major and Minor Isoform Peptides")+
        xlab("Dataset")+ylab("Score")+theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust=0.5))+coord_flip()
    g
    })
    
############################
#STUFF FOR COVERAGE PLOTS
############################    
    observe({
        print("annot")
        annotationFile<-input$annotation
        if(is.null(annotationFile)){
            return (NULL)
        }
        annot<-import(annotationFile$datapath, format = "gtf")
        annot<-annot[annot@elementMetadata$type=="CDS" & annot@elementMetadata$gene_type=="protein_coding", c("gene_id", "gene_name", "transcript_id", "protein_id")]
        if(isolate(input$proteome_type=="UniProtKB")){
            print("hohoho")
            annot<-adjustAnnot(annot, mappingFile)
        }
        if(!is.null(proteome)){
            if(isolate(input$proteome_type=="UniProtKB")){
                annot<-annot[annot@elementMetadata$protein_name %in% proteome$transcript]
                proteome<<-proteome[transcript %in% annot@elementMetadata$protein_name]
                }
            else{
            annot<-annot[annot@elementMetadata$transcript_id %in% proteome$transcript]
            proteome<<-proteome[transcript %in% annot@elementMetadata$transcript_id]
            }
            updateSelectizeInput(session, "gene", label ="Select Gene", choices = unique(proteome$gene), server=T)
        }
        print(annot)
        annotation<<-annot
        })
    #get proteome file into global context
    observe({
        #print("prot")
        proteomeFile<-input$proteome
        if(is.null(proteomeFile)){
            return(NULL)
        }
        prot<-fread(proteomeFile$datapath, header=T, sep="\t")
        if(!is.null(annotation)){
            #print("gna")
            print(annotation)
            if(isolate(input$proteome_type=="UniProtKB")){
                prot<-prot[transcript %in% annotation@elementMetadata$protein_name]
            }
            else{
            prot<-prot[transcript %in% annotation@elementMetadata$transcript_id]
            }
        }
        p<-prot[, .N, by=c("gene")]
        gen<-p[N>1,]$gene
        #print(gen)
        prot<-prot[gene %in% gen,]
        cho<-unique(prot$gene)
        updateSelectizeInput(session, "gene", label ="Select Gene", choices = cho, server=T)
        proteome<<-prot
    })
    observe({
    ge<-input$gene
     if(ge!=""){
       cho<-unique(proteome[gene==ge, transcript])
       updateSelectizeInput(session, "iso1", label ="Select Isoform", choices=cho, server = T)
       updateSelectizeInput(session, "iso2", label ="Select Isoform", choices=cho, server = T)
    }
    })
    observe({
        if(!is.null(input$coverage)){
        cov<-fread(input$coverage$datapath, header=T, sep="\t")
        }
        coverage<<-cov
    })
    computeCov<-reactive({
        input$click
        iso2<-isolate(input$iso2)
        if(!is.null(annotation)){
            if(!is.null(proteome)){
                if(iso2!=""){
                    if(isolate(input$proteome_type=="UniProtKB")){
                        bool<-TRUE
                    } 
                    else{
                        bool<-FALSE
                    }
                    co<-isolate(generateCoveragePlot(input$gene, input$iso1, input$iso2, annotation, proteome, coverage, bool))
                    m<-nchar(co$seq1@trackList[[1]]@sequence[1])
                    updateSliderInput(session, "window", label = "Window", min = 1, max = m, value=c(1,m))
                    co
                }
            }
        }})
     output$coveragePlot<-renderPlot({
         if(!is.null(input$iso2) && !is.null(annotation) && !is.null(proteome)){
         co<-computeCov()
         i<-isolate(input$window)
         print(i)
         # print(names(co))
         m<-i[2]
         a<-i[1]
         plotTracks(co, from = a, to=m, fontcolor=fcol, sizes = rep(1, length(co)))
         }
     })
     observe({
         print(input$proteome_type)
     })
     
})

