setwd("/home/nick/neap/git/NEAP")
require(data.table)
library(gridExtra)


folder_wilhelm <- "/home/nick/neap/git/NEAP/results/08_identify_splicing_events/wilhelm/"   # path to folder that holds multiple evidence files
file_list_wilhelm <- list.files(path=folder_wilhelm,recursive = T, pattern="*.txt") # create list of all evidence files in folder

folder_kim <- "/home/nick/neap/git/NEAP/results/08_identify_splicing_events/kim/"   # path to folder that holds multiple evidence files
file_list_kim <- list.files(path=folder_kim,recursive = T, pattern="*.txt") # create list of all evidence files in folder

shared_evidences<-intersect(file_list_wilhelm,file_list_kim)  #only use shared evidence files (e.g. adrenal.gland)

for (i in shared_evidences){
  j<-strsplit(i,"/")[[1]][1] #shorten string to tissue name
  j_kim<-paste("kim_",j,sep = '')
  j_wil<-paste("wil_",j,sep = '')
  assign(j_kim, read.delim(paste("./results/08_identify_splicing_events/kim/",i,sep = '')))
  assign(j_wil, read.delim(paste("./results/08_identify_splicing_events/wilhelm/",i,sep = '')))
}

for (i in shared_evidences){
  ## variable names ##
  j<-strsplit(i,"/")[[1]][1] #shorten string to tissue name
  j_kim_unique<-paste("kim_unique_",j,sep = '')
  j_wil_unique<-paste("wil_unique_",j,sep = '')
  j_kim_all<-paste("kim_all_",j,sep = '')
  j_wil_all<-paste("wil_all_",j,sep = '')
  j_kim_wil_intersect<-paste("kim_wil_intersect_",j,sep = '')
  j_all_col<-paste("all_",j,sep = '')
  ##
  
  ## get tissue specific information & save in dataframe ##
  assign(j_kim_unique, length(unique(get(paste("kim_",j,sep = ''))$Peptide)))
  assign(j_kim_unique, data.frame(j=get(j_kim_unique),row.names = "Kim: # of unique peptides"))
  tmp<-get(j_kim_unique)
  colnames(tmp)<-j
  assign(j_kim_unique,tmp)
  assign(j_wil_unique, length(unique(get(paste("wil_",j,sep = ''))$Peptide)))
  assign(j_wil_unique, data.frame(j=get(j_wil_unique),row.names = "Wilhelm: # of unique peptides"))
  tmp<-get(j_wil_unique)
  colnames(tmp)<-j
  assign(j_wil_unique,tmp)
  
  assign(j_kim_all, length(get(paste("kim_",j,sep = ''))$Peptide))
  assign(j_kim_all, data.frame(j=get(j_kim_all),row.names = "Kim: # of peptides"))
  tmp<-get(j_kim_all)
  colnames(tmp)<-j
  assign(j_kim_all,tmp)
  assign(j_wil_all, length(get(paste("wil_",j,sep = ''))$Peptide))
  assign(j_wil_all, data.frame(j=get(j_wil_all),row.names = "Wilhelm: # of peptides"))
  tmp<-get(j_wil_all)
  colnames(tmp)<-j
  assign(j_wil_all,tmp)
  
  assign(j_kim_wil_intersect, length(intersect(get(paste("kim_",j,sep = ''))$Peptide,get(paste("wil_",j,sep = ''))$Peptide)))
  assign(j_kim_wil_intersect, data.frame(j=get(j_kim_wil_intersect),row.names = "Kim&Wilhelm: # of intersecting peptides"))
  tmp<-get(j_kim_wil_intersect)
  colnames(tmp)<-j
  assign(j_kim_wil_intersect,tmp)
  ##
  
  assign(j_all_col, rbind(get(j_kim_all),get(j_wil_all),get(j_kim_unique),get(j_wil_unique),get(j_kim_wil_intersect)))  #rbind tissue specific
  #all<-cbind(get(j_kim_all),get(j_wil_all),get(j_kim_unique),get(j_wil_unique),get(j_kim_wil_intersect))
}

# read in each evidence file in file_list and rbind them into a data frame called data 
data_kim <- 
  do.call("rbind", 
          lapply(file_list_kim, 
                 function(x) 
                   read.csv(paste(folder_kim, x, sep=''), 
                            sep = "\t",header=T,fill=T, row.names = NULL)))

data_wilhelm <- 
  do.call("rbind", 
          lapply(file_list_wilhelm, 
                 function(x) 
                   read.csv(paste(folder_wilhelm, x, sep=''), 
                            sep = "\t",header=T,fill=T, row.names = NULL)))

## process combined datasets ##
combined_kim<-length(data_kim$Peptide)
combined_kim<-data.frame(Combined=combined_kim,row.names = "Kim: # of peptides")
combined_wilhelm<-length(data_wilhelm$Peptide)
combined_wilhelm<-data.frame(Combined=combined_wilhelm,row.names = "Wilhelm: # of peptides")

kim_combined_unique_peptides<-length(unique(data_kim$Peptide))
kim_combined_unique_peptides<-data.frame(Combined=kim_combined_unique_peptides,row.names = "Kim: # of unique peptides")
wil_combined_unique_peptides<-length(unique(data_wilhelm$Peptide))
wil_combined_unique_peptides<-data.frame(Combined=wil_combined_unique_peptides,row.names = "Wilhelm: # of unique peptides")

int<-length(intersect(data_kim$Peptide,data_wilhelm$Peptide))
int<-data.frame(Combined=int,row.names = "Kim&Wilhelm: # of intersecting peptides")

combined_col<-rbind(combined_kim,combined_wilhelm,kim_combined_unique_peptides,wil_combined_unique_peptides,int)
##

complete<-cbind(all_adrenal.gland,all_colon,all_esophagus,all_kidney,all_liver,all_lung,all_ovary,all_pancreas,all_prostate,combined_col) #combine everything into one df
pdf("./results/08_identify_splicing_events/compare_kim_wil.pdf", height = 3, width = 12)
grid.table(complete)
dev.off()
write.table(complete, "./results/08_identify_splicing_events/compare_kim_wil.txt", sep="\t")

test<-as.data.frame(kim_all_adrenal.gland,row.names = "# of Kim all")
colnames(test)<-"Adrenal.gland"
test1<-as.data.frame(wil_all_adrenal.gland,row.names = "# of Wilhelm all")
colnames(test1)<-"Adrenal.gland"
test
test1
rbind(test,test1)




int_u<-unique(int)
