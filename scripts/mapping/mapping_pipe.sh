#!/bin/bash

# script to create mapping for every evidence file in /kim and /wilhelm
# TODO: mkdir evidences! -> mkdir -p
i_end=""
data=""

for i in $1/*.txt; do 	#kim data set

	i_end=${i##*/}		#cut for example ../data/kim/pancreas_evidence.txt -> pancreas_evidence.txt
	i_end_no_evidence_dot_txt=${i_end%_*} 	#cut pancreas_evidence.txt -> pancreas
        i_end_folder=${1##*/}
        output=$i_end_folder"_"$i_end_no_evidence_dot_txt"_transcript_gene_disc_mapping.tsv"
        data=$i_end_folder

 	for j in 1 2 3; do
		if [ $j = 1 ]; then
		      dummy="yes"
        elif [ $j = 2 ]; then
                mkdir -p ./02_mapping_to_evidences/trypsin/$i_end_folder
        	perl $2/scripts/mapping/peptide_mapping.pl ./01_in_silico_digest/trypsin/transcript_gene_disc.tsv $i ./02_mapping_to_evidences/trypsin/$i_end_folder/$output $j
        elif [ $j = 3 ]; then
        	dummy="yes"
        fi
	done
done

echo "========== Mapping data set $data finished =========="
