require(data.table)
require(stringr)
require(ggplot2)
require(RColorBrewer)
require(optparse)
option_list <- list(
  make_option(c("-p", "--peptides"), type="character",
              help="peptide files, separated by colon"),
  make_option(c("-e", "--empirial"), type="character",
              help="empirical peptide file"),
  make_option(c("-n", "--names"), type="character",
              help="names of the proteases for each file"),
  make_option(c("-o", "--outfile"), type="character",
              help="output filename")
)
opt <- parse_args(OptionParser(option_list=option_list))
opt$peptides<-"/home/proj/biocluster/praktikum/neap_pearl/hartebrodt_lehner/project_data/results_ensembl/01_in_silico_digest/trypsin/trypsin_peptides_unique.tsv"
opt$names<-"trypsin"
opt$empirical<-"/home/proj/biosoft/praktikum/neap_pearl/projects/proteomics/Kim_Rerun_Ensembl/rerun_pancreas_bRP_evidence.txt"
files<-unlist(str_split(opt$peptides, ":"))
files<-files[files!=""]
proteases<-unlist(str_split(opt$names, ":"))
proteases<-proteases[proteases!=""]
stopifnot(length(files)==length(proteases))
da<-list()
for (f in 1:length(files)){
  dataset<-fread(files[f], sep="\t")
  #dataset$dataset<-proteases[f]
  da[[proteases[f]]]<-dataset
}


emp<-fread(opt$empirical, sep="\t")
emp<-emp[,1]
colnames(emp)<-c("Peptide")
len<-function(x){
    x[,len:=nchar(Peptide)]
}
da<-lapply(da, function(x) len(x))

pal<-brewer.pal(n=length(proteases), name="Spectral")
names(pal)<-proteases
df<-do.call(cbind, da)
df<-as.data.table(df)
colnames(df)<-proteases
df<-melt(df)
df$variable<-sapply(df$variable, function(x) gsub("\\.len", "", x))

g<-ggplot(df, aes(x=value,color=variable)) +geom_step(stat="ecdf")+
    geom_vline(xintercept = 6)+
    scale_colour_manual("Protease", values=pal)+
  coord_cartesian(xlim=c(0,50))+
    ggtitle("Peptide length distribution\nin-silico digests vs. empirial distribution")+
        ylab("%Peptides")+xlab("Peptide length")+theme(text = element_text(size=20))
png(file.path(dirname(opt$outfile), "in_silico_digest_pep_len.png"), width = 900, height = 600)
plot(g)
dev.off()

fwrite(df, outfile, sep="\t")

#g<-ggplot(emp, aes(len))+geom_step(stat = "ecdf")+xlim(0,45)+xlab("Peptide length")+ylab("#Peptides")+
#    ggtitle("Cumulative distribution of peptide length in experimental evidence")+theme(text = element_text(size=20))

#png("~/NEAP/NEAP/presentation/empirical_peptide_length.png", width = 900, height = 600)
#plot(g)
