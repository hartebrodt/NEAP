from Bio import SeqIO
import ahocorasick
import os.path
import argparse
parser = argparse.ArgumentParser(description='Read Coverage')
parser.add_argument('inputfile', metavar="Input", type=str,
                                        help='evidence file')
parser.add_argument('outdir', metavar="Input", type=str,
                                        help='proteome file')

args = parser.parse_args()
outdir=args.outdir

A = ahocorasick.Automaton()
silico={}
ppg={}
with open(args.inputfile) as handle:
    for line in handle:
        l=line.rstrip().split("\t")
        for pep in l[2].split("|"):
            if pep not in A:
                A.add_word(pep, {l[0]:set([l[1]])})
            else:
                if l[0] not in A.get(pep).keys():
                    A.get(pep)[l[0]]=set([l[1]])
                else:
                    A.get(pep)[l[0]].add(l[1])
        if l[0] not in ppg.keys():
            ppg[l[0]]=set([l[1]])
        else:
            ppg[l[0]].add(l[1])
          

A.make_automaton()

igene=[]
igene_long=[]
itrans=[]
itrans_long=[]
nutrans=[]
nutrans_long=[]
nugene=[]
nugene_long=[]
gen_trans_uniq=[]
gen_trans_uniq_long=[]
for pep in A.keys():
    if len(A.get(pep).keys())==1:
        if len(pep)<50:
            igene.append([list(A.get(pep).keys())[0], pep])
        else:
            igene_long.append([list(A.get(pep).keys())[0], pep])
    else:
        if len(pep)<50:
            nugene.append([list(A.get(pep).keys()), pep])
        else:
            nugene_long.append([list(A.get(pep).keys()), pep])
    for gene in A.get(pep).keys():
		# Peptide must not exsist in at least one isoform of the gene
		# Transcript list in ppg must be longer than the transcript list
		# in the search tree.
        if len(A.get(pep)[gene])<len(ppg[gene]):
            # If only one transcript exists for this gene (no isoforms) exclude it
            #if len(ppg[gene])>1: obsolete
            if len(pep)<50:
                itrans.append([gene, A.get(pep)[gene] , pep])
            else:
                itrans_long.append([gene,  A.get(pep)[gene], pep])
            if len(A.get(pep).keys())==1:
                if len(pep)<50 :
                    gen_trans_uniq.append([gene, A.get(pep)[gene], pep])
                else:
                    gen_trans_uniq_long.append([gene,  A.get(pep)[gene], pep])
                        
                
        else:
            if len(pep)<50:
                nutrans.append([gene, A.get(pep)[gene], pep])
            else:
                nutrans_long.append([gene, A.get(pep)[gene], pep])
            

#with open(os.path.join("summary.tsv"), "w") as handle:
 #   handle.write("identifies gene"+str(identifies_gene)+"\n")
  #  handle.write("not identifies gene"+str(not_id_gen)+"\n")
   # handle.write("identifies transcript"+str(identifies_transcript)+"\n")
   # handle.write("identifies transcript proteome unique"+str(id_trans_prot_unique)+"\n")
   # handle.write("not identifies transcript"+str(no_not_id)+"\n")
   # handle.write("concerned transcripts"+str(not_id_trans)+"\n")

def write123(filename, cols, data):
    with open(filename, "w") as handle:
        for i in data:
            for j in range(0, cols):
                handle.write(i[j]+"\t")
            handle.write(i[cols]+"\n")
def writeSplit(filename, cols, data, split):
    with open(filename, "w") as handle:
        for i in data:
            for j in range(0,split):
                handle.write(i[j]+"\t")
            for j in i[split]:
                handle.write(j+"|")
            for j in range(split+1, cols):
                handle.write("\t"+i[j])
            handle.write("\n")
    

writeSplit(os.path.join(outdir, "transcript_unique.tsv"), 3, itrans, 1)
writeSplit(os.path.join(outdir, "transcript_unique_long.tsv"), 3, itrans_long, 1)
writeSplit(os.path.join(outdir, "transcript_unique_proteome.tsv"), 3, gen_trans_uniq, 1)
writeSplit(os.path.join(outdir, "transcript_unique_proteome_long.tsv"), 3, gen_trans_uniq_long, 1)
write123(os.path.join(outdir, "gene_unique.tsv"), 1, igene)
write123(os.path.join(outdir, "gene_unique_long.tsv"), 1, igene_long)
writeSplit(os.path.join(outdir, "not_unique_transcript.tsv"), 3, nutrans, 1)
writeSplit(os.path.join(outdir, "not_unique_transcript_long.tsv"), 3, nutrans_long, 1)
writeSplit(os.path.join(outdir, "not_unique_gene.tsv"), 2, nugene, 0)
writeSplit(os.path.join(outdir, "not_unique_gene_long.tsv"), 2, nugene_long, 0)

