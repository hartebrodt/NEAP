require(data.table)
require(cleaver)
require(stringr)
da<-fread("/home/anne/NEAP/project/data/testfile.txt", header=F, sep="\t")
sub<-lapply(da$V3, function(x) strsplit(x, "|", fixed = T))
sub_u<-unique(sub)
res<-data.table(V1=character(), V2=character(), V3=integer())
l<-c()
for (i in 1:19){
    for (j in (i+1):20){
        r1<-setdiff(unlist(sub[i]), unlist(sub[j]))
        r2<-setdiff(unlist(sub[j]), unlist(sub[i]))
        l<-c(r1,r2,l)
        res<-rbind(res, list(i, j, (length(r1)+length(r2))))
    }
}
# Man muss noch die zu langen und die zu kurzen Peptide rausschmeißen
long<-length(which(sapply(l, function(x) str_count(x)>50)))
s<-length(which(sapply(l, function(x) str_count(x)<6)))
v2<-res$V3
sum(v2)
sum(v2)-s-long

# Gen: ENSG00000070756.15
# Dieser Vector kommt aus dem result file, dass auf dem Cluster liegt und zwar trancript_dic.tsv
# da habe ich dann das selbe gen rausgenommen, was ich in testfile habe und jeweils gezählt
# wieviel peptide ich pro transcript paar finde. Überprüfe das bitte auch nochmal!
v<-c(75, 54,159 ,78,24,26,22,161,17,169,55,123,52,60,25,160,36,60,
33,63,138,103,57,57,85,130,64,148,64,106,73,99,88,153,51,93,
54,153,90,34,32,64,137,43,149,5,135,22,78,67,160,24,72,27,
125,169,167,143,48,164,22,154,90,169,143,140,33,159,149,162,60,62,
88,117,67,135,91,73,88,72,91,126,72,92,69,6,34,163,13,179,
35,105,34,48,37,172,16,42,13,36,163,15,177,33,107,36,50,39,
174,14,44,11,159,27,153,65,133,62,58,9,144,46,70,43,156,26,
132,82,131,167,162,31,157,141,160,174,44,112,41,55,30,165,25,49,
22,144,100,149,153,150,11,169,159,172,136,17,79,68,155,25,73,28,
131,117,136,91,117,93,114,76,65,138,42,70,43,55,144,60,76,57,
141,49,73,46,180,150,181,54,9,51)

sum(v)
